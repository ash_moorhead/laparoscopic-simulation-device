from serial import *
import struct
import matplotlib.pyplot as plt
from datetime import *
import matplotlib.animation as animation

ser = Serial('COM5', 250000, timeout=1)
print(ser)
if not ser.is_open:
    ser.open()

fig, ax = plt.subplots()
ax.set_xlim(0, 30)
ax.set_ylim(-2, 2)

t = [0.0]
x = [0.0]
line, = ax.plot(t, x)
start = datetime.now()


def animate(i):
    c = ser.read(1)
    if c is b'd':
        first = ser.read(4)
        first, = struct.unpack('l', first)

        # PMW3360 #
        # read four readings
        for i in range(0, 4):
            # read left and right readings
            for j in range(0, 2):
                # read X and Y shorts
                for k in range(0, 2):
                    b = ser.read(2)
                    short, = struct.unpack('<h', b)
                    if k == 1 and j == 0: # Y and Left:
                        x.append(short)
                        t.append((first + 5*i)/1000)
                # read Squal unsigned char
                s = ser.read(1)
                # discard
                # squal, = struct.unpack('B', s)

    line.set_data(t, x)
    return line,


def init():
    initL, = ax.plot([], [])
    return initL,


ani = animation.FuncAnimation(fig, animate, 50, init_func=init,
                              interval=5, blit=True)
plt.show()


