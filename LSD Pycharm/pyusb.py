import usb.core
import usb.util
import time

intf = 0
dev = usb.core.find(idVendor=0x256F, idProduct=0xC62F)


def main():

        if dev is None:
            print("device not found")

        else:
            print("Device found")
            for cfg in dev:
                print("Config: " + str(cfg))
            # use default configuration (spacemouse only has one)
            dev.set_configuration()

            # get an endpoint
            cfg = dev.get_active_configuration()

            ep = usb.util.find_descriptor(
                cfg[(0, 0)],
                # match the first IN endpoint
                custom_match=\
                lambda en: \
                usb.util.endpoint_direction(en.bEndpointAddress) == \
                usb.util.ENDPOINT_IN)

            assert ep is not None
            print(ep.wMaxPacketSize)

            # force = []
            # read data
            while True:
                try:
                    data = dev.read(ep.bEndpointAddress,
                                    ep.wMaxPacketSize)
                    # temp = [time.time()]
                    # for i in range(2, len(data), 2):
                    #     temp.append(to_signed(data[i])*256 + data[i-1])
                    # force.append(temp)

                    print('Fx:' + str(to_signed(data[2]) * 256 + data[1])
                          + '   Tx:' + str(to_signed(data[8]) * 256 + data[7])
                          + '   Fy:' + str(to_signed(data[4]) * 256 + data[3])
                          + '   Ty:' + str(to_signed(data[10]) * 256 + data[9])
                          + '   Fz:' + str(to_signed(data[6]) * 256 + data[5])
                          + '   Tz:' + str(to_signed(data[12]) * 256 + data[11]))

                except usb.core.USBError as e:
                    if e.args == (10060, u'Operation timed out'):
                        continue
                    else:
                        print('error' + str(e.args))

            # return 0


def toNewtons(byte1, byte2, Cal):
    return Cal(to_signed(byte2) * 256 + byte1)


def xCal(Rx):
    return (Rx + 7.1667)/177.49


def to_signed(byte):
    if byte > 127:
        return (256 - byte) * (-1)
    else:
        return byte


if __name__ == '__main__':
    main()
