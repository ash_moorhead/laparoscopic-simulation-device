from PyQt5 import QtGui, QtWidgets, QtCore
from serial import Serial, SerialException
import cv2
import usb.core
import usb.util
from usb import USBError
from PyQt5.QtCore import QThread, QObject, pyqtSignal
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.artist
from matplotlib.lines import Line2D
from matplotlib.patches import FancyArrowPatch
from matplotlib.animation import FuncAnimation, Animation
from mpl_toolkits.mplot3d import proj3d, Axes3D
from datetime import datetime
import time
import os
from math import atan2, asin
import struct


# https://stackoverflow.com/questions/22867620/putting-arrowheads-on-vectors-in-matplotlibs-3d-plot
# patch for 3D arrow
class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0, 0), (0, 0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0], ys[0]), (xs[1], ys[1]))
        FancyArrowPatch.draw(self, renderer)


class Capture(QObject):

    frameRead = pyqtSignal(object)

    def __init__(self):
        super(Capture, self).__init__()

        # define video capture and open
        self.cap = cv2.VideoCapture()
        if self.cap.isOpened() is False:
            self.cap.open(0)

        # Define the recording variables
        self.out = None
        self.vidName = None
        self.recording = False
        self.timer = None
        self.thread = None

        self.fps = self.getFrameRate()

    def read(self):
        ret, frame = self.cap.read()
        # start = datetime.now()
        if self.recording:
            self.out.write(frame)
            # print("frame", datetime.now())
        height, width, channel = frame.shape
        bytesperline = 3 * width
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        qImg = QtGui.QImage(frame.data, width, height, bytesperline, QtGui.QImage.Format_RGB888)
        pix = QtGui.QPixmap(qImg)
        self.frameRead.emit(pix)
        # print((datetime.now() - start).total_seconds())

    def save(self):
        if self.recording:
            # restarts videowriter with new file (effectively flushing old videowriter)
            try:
                self.out.release()
            except Exception as e:
                print(e)

            self.recording = False

    def getFrameRate(self):
        # Start time
        start = time.time()

        # Grab a few frames
        for i in range(0, 30):
            ret, frame = self.cap.read()

        # End time
        end = time.time()

        # Time elapsed
        seconds = end - start

        # Calculate frames per second
        fps = 30 / seconds
        print("Estimated frames per second : {0}".format(fps))
        return fps

    def record(self, fold):
        # create new videoWriter if not initialised or released
        if self.out is None or not self.out.isOpened():
            # self.vidName = Capture.getFileName("avi")
            self.vidName = fold + Capture.getFileName("avi")
            print(self.vidName)
            fourcc = cv2.VideoWriter_fourcc(*'XVID')
            self.out = cv2.VideoWriter(self.vidName, fourcc, self.fps,
                                       (int(self.cap.get(cv2.CAP_PROP_FRAME_WIDTH)),
                                        int(self.cap.get(cv2.CAP_PROP_FRAME_HEIGHT))))
        self.recording = True
        print('recording')

    def stop(self):
        if self.recording:
            self.save()
        self.timer.stop()
        self.cap.release()
        self.thread.quit()

    @staticmethod
    def getFileName(ext):
        seq = (str(datetime.now().strftime("%Y-%m-%d-%H%M%S")), ext)
        return ".".join(seq)


class Spacemouse(QObject):
    """Object Controlling the reading and other initialising and communicating with the spacemouse 3D mouse"""

    dataRead = pyqtSignal(list, name='dataRead')

    def __init__(self):
        super(Spacemouse, self).__init__()

        # find spacemouse (Wired is idProduct=0xC62E)
        try:
            self.dev = usb.core.find(idVendor=0x256F, idProduct=0xC62F)
        except USBError as e:
            self.dev = None
            print("Spacemouse not found! ", e)
            return
        if self.dev is None:
            print("Spacemouse not found... If it is connected try changing USB ports.")
            return
        self.fForce = None
        self.recording = False
        self.startTime = datetime.now()
        self.timer = None
        self.thread = None

        # configure USB and get endpoint
        print("Device found")

        # use default configuration (spacemouse only has one)
        self.dev.set_configuration()

        # get an endpoint
        config = self.dev.get_active_configuration()

        self.ep = usb.util.find_descriptor(
            config[(0, 0)],
            # match the first IN endpoint
            custom_match=
            lambda en:
                usb.util.endpoint_direction(en.bEndpointAddress) == \
                usb.util.ENDPOINT_IN)

    def record(self, fold):
        if not self.recording:
            if self.fForce is not None:
                self.fForce.close()
            fileName = fold + "Space_" +Capture.getFileName("csv")
            self.fForce = open(fileName, 'w')
            self.startTime = datetime.now()
            self.recording = True

    def save(self):
        if self.recording:
            self.recording = False

    def read(self):
        # read data
        try:
            data = self.dev.read(self.ep.bEndpointAddress,
                                 self.ep.wMaxPacketSize)

            now = datetime.now() - self.startTime

            # write all force data to .csv file
            if self.recording:
                # print("val", datetime.now())
                self.fForce.write(",".join([str(now.total_seconds())
                                  , str(self.to_signed(data[2]) * 256 + data[1])
                                  , str(self.to_signed(data[4]) * 256 + data[3])
                                  , str(self.to_signed(data[6]) * 256 + data[5])
                                  , str(self.to_signed(data[8]) * 256 + data[7])
                                  , str(self.to_signed(data[10]) * 256 + data[9])
                                  , str(self.to_signed(data[12]) * 256 + data[11])]) + "\n")
                self.fForce.flush()

            # Signal to main thread to plot translational forces
            self.dataRead.emit([now.total_seconds(),
                                self.toNewtons(data[7], data[8], Spacemouse.xCal),
                                self.toNewtons(data[9], data[10], Spacemouse.yCal),
                                self.toNewtons(data[5], data[6], Spacemouse.zCal)])

        except USBError as e:
            if e.args != (10060, u'Operation timed out'):
                print('error' + str(e.args))

    def stop(self):
        self.timer.stop()
        if self.fForce is not None:
            self.fForce.close()
        usb.util.dispose_resources(self.dev)
        self.thread.quit()

    @staticmethod
    def to_signed(byte):
        if byte > 127:
            return (256 - byte) * (-1)
        else:
            return byte

    def toNewtons(self, byte1, byte2, calFunc):
        return calFunc(self.to_signed(byte2) * 256 + byte1)

    @staticmethod
    def xCal(Rx):
        return (Rx + 7.1667) / 177.49

    @staticmethod
    def yCal(Ry):
        return (Ry + 0.8333) / -188.52

    @staticmethod
    def zCal(Ry):
        return (Ry + 7.5714) / 97.444


class Arduino(QObject):
    """Object controlling the polling of the Arduino via serial for the four connected sensor readings"""

    dataRead = pyqtSignal(list, name='orientationRead')

    def __init__(self):
        super(Arduino, self).__init__()

        try:
            self.ser = Serial('COM5', 250000, timeout=1)
            if not self.ser.is_open:
                self.ser.open()
        except SerialException as e:
            print("Serial port could not be opened, Ensure Arduino is connected "
                  "and no other instances are still running!", e)
            self.ser = None
            return
        print("serial open")

        self.recording = False
        self.startTime = datetime.now()
        self.fPort = None  # file for recording orientation data
        self.timer = None
        self.thread = None
        self.printing = False
        # [dx, dy]
        self.xy = [[0, 0], [0, 0]]

    def record(self, fold):
        if not self.recording:
            if self.fPort is not None:
                self.fPort.close()
            fileName = fold + "Port_" + Capture.getFileName("csv")
            self.fPort = open(fileName, 'w')
            self.fPort.write("t, LX1, LY1, Ls1, RX1, RY1, Rs1, LX2, LY2, Ls2, RX2, RY2, Rs2, "
                             "LX3, LY3, Ls3, RX3 ,RY3, Rs3, LX4, LY4, Ls4, RX4, RY4, Rs4, Lw, "
                             "lx, Ly, Lz, Rw, Rx, Ry, Rz\n")

            self.fPort.flush()
            self.startTime = datetime.now()
            # if there is a displacement that hasn't been written to file write the two dx, dy values to file first
            if not all(v == 0 for v in self.xy):
                for item in self.xy:
                    self.fPort.write("%s," % item)
                self.fPort.write("\n")
            self.recording = True

    def reset_orientation(self):
        return self.ser.write(struct.pack('b', int(1)))

    def save(self):
        if self.recording:
            self.xy = [[0, 0], [0, 0]]
            self.recording = False

    def toggle_print(self):
        self.printing = not self.printing

    def stop(self):
        self.timer.stop()
        self.ser.close()
        if self.fPort is not None:
            self.fPort.close()
        self.thread.quit()

    def read(self):
        # read data
        try:
            strng = None
            c = self.ser.read(1)
            if c is b'd':

                t = self.ser.read(4)
                t, = struct.unpack('l', t)
                t = t/1000
                strng = str(t) + ","

                # PMW3360 #
                # read four readings
                for i in range(0, 4):
                    # read left and right readings
                    for j in range(0, 2):
                        # read X and Y shorts
                        for k in range(0, 2):
                            b = self.ser.read(2)
                            short, = struct.unpack('<h', b)
                            strng += str(short) + ","

                            if short != 0 and short > 2:
                                self.xy[j][k] += short
                        # read Squal unsigned char
                        s = self.ser.read(1)
                        squal, = struct.unpack('B', s)
                        strng += str(squal) + ","

                # BNO055 #
                bno = [[t], [t]]  # timestamp first
                # Read quaternion for left and right orientation sensors
                for i in range(0, 2):
                    # read w, x, y, z of the quaternion
                    bno[i].append(i)  # tell which is left
                    for j in range(0, 4):
                        b = self.ser.read(4)
                        flt, = struct.unpack('f', b)
                        strng += str(flt) + ","
                        bno[i].append(flt)

                    # Signal to main thread to plot translational forces
                    self.dataRead.emit(bno[i])

                # write all force data to .csv file
                if self.recording:
                    # written as time, LdX1, LdY1, Ls1, LdX2, LdY2, Ls2, RdX1, RdY1, Rs1, RdX2, RdY2, Rs2,
                    # Lw, Lx, Ly, Lz, Rw, Rx, Ry, Rz
                    if not self.fPort.closed:
                        self.fPort.write(strng + "\n")
                        self.fPort.flush()

                if self.printing:
                    print(strng)

            elif c == b'e':
                print('error on Arduino: %s' % self.ser.read(1))
            else:
                print("filtered: %s " % c)

        except Exception as e:
            print('Serial read error!', e)
            if strng is not None:
                print(strng)


# the actual work performed by the thread must be in a separate QObject and initialised by the worker thread, otherwise
# the work will be performed by the main thread and only the timer will be run on a separate thread.
# http://ilearnstuff.blogspot.co.nz/2012/08/when-qthread-isnt-thread.html
class SensorThread(QThread):
    def __init__(self, sensor, interval):
        QThread.__init__(self)
        sensor.moveToThread(self)
        self.sensor = sensor
        self.interval = interval

    def __del__(self):
        self.wait()

    def run(self):
        # start Timer and initialse sensor Object
        timer = QtCore.QTimer()
        timer.setSingleShot(False)
        timer.timeout.connect(self.sensor.read)
        timer.start(self.interval)
        self.sensor.timer = timer
        self.sensor.thread = self
        self.exec_()


class VideoDisplayWidget(QtWidgets.QWidget):
    def __init__(self, parent, fps):
        super(VideoDisplayWidget, self).__init__(parent)

        # properties
        self.qPix = None  # Stores most recent frame
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.setFrame)
        self.timer.start(1000/fps)
        # Initialise buttons
        self.recordButton = QtWidgets.QPushButton('Record', parent)
        self.printButton = QtWidgets.QPushButton('PrintVal', parent)
        self.textbox = QtWidgets.QLineEdit(self)
        self.textbox.resize(250, 30)
        self.video_frame = QtWidgets.QLabel()
        self.videoLabel = QtWidgets.QLabel('', parent)

        self.hbox = QtWidgets.QHBoxLayout()
        self.hbox.addWidget(self.recordButton)
        self.hbox.addWidget(self.textbox)
        self.hbox.addWidget(self.printButton)
        self.hbox.addWidget(self.videoLabel)
        self.hbox.addStretch(1)

        self.vbox = QtWidgets.QVBoxLayout()
        self.vbox.addLayout(self.hbox)
        self.vbox.addWidget(self.video_frame)

        self.setLayout(self.vbox)

    def updateFrame(self, qPix):
        self.qPix = qPix

    def setFrame(self):
        if self.qPix is not None:
            self.video_frame.setPixmap(self.qPix)


class Qt5MplCanvas(FigureCanvas):
    """Class to represent the FigureCanvas widget"""

    def __init__(self, parent, title, ylabel, ylims):
        # Properties
        self.figure = Figure()

        FigureCanvas.__init__(self, self.figure)

        self.axes_x = self.figure.add_subplot(311)
        self.axes_y = self.figure.add_subplot(312)
        self.axes_z = self.figure.add_subplot(313)

        self.axes_x.set_xlabel('time(s)')
        self.axes_x.set_ylabel(ylabel)
        self.axes_x.set_xlim(0, 30)
        self.axes_x.set_ylim(ylims)
        self.axes_x.set_autoscale_on(False)

        self.axes_y.set_xlabel('time(s)')
        self.axes_y.set_ylabel(ylabel)
        self.axes_y.set_xlim(0, 30)
        self.axes_y.set_ylim(ylims)
        self.axes_y.set_autoscale_on(False)

        self.axes_z.set_xlabel('time(s)')
        self.axes_z.set_ylabel(ylabel)
        self.axes_z.set_xlim(0, 30)
        self.axes_z.set_ylim(ylims)
        self.axes_z.set_autoscale_on(False)

        self.figure.suptitle(title)

        self.setParent(parent)

        # we define the widget as expandable
        FigureCanvas.setSizePolicy(self,
                                   QtWidgets.QSizePolicy.Expanding,
                                   QtWidgets.QSizePolicy.Expanding)
        # notify the system of updated policy
        FigureCanvas.updateGeometry(self)


class ForceDisplayWidget(QtWidgets.QWidget):
    def __init__(self, parent):
        super(ForceDisplayWidget, self).__init__(parent)

        # # Properties
        #  Plot data sources
        self.x = [0]
        self.y = [0]
        self.z = [0]
        self.t = [0]
        self.currentFrame = 1
        self.intervalSize = 30   # in seconds

        self.line_x = Line2D([], [], color='blue')
        self.line_y = Line2D([], [], color='red')
        self.line_z = Line2D([], [], color='magenta')

        self.label = QtWidgets.QLabel('', parent)
        self.forcePlot = Qt5MplCanvas(self, 'Force X, Y, Z', 'force (N)', (-4, 4))

        self.anim = None

        self.forcePlot.axes_x.add_line(self.line_x)
        self.forcePlot.axes_y.add_line(self.line_y)
        self.forcePlot.axes_z.add_line(self.line_z)

        self.forcePlot.axes_x.xaxis.set_animated(True)
        self.forcePlot.axes_y.xaxis.set_animated(True)
        self.forcePlot.axes_z.xaxis.set_animated(True)

        self.layout = QtWidgets.QVBoxLayout()
        self.layout.addWidget(self.label)
        self.layout.addWidget(self.forcePlot)
        self.forcePlot.figure.canvas.draw()
        self.setLayout(self.layout)

    def start_anim(self):
        Animation._blit_draw = self._blit_draw
        self.anim = FuncAnimation(self.forcePlot.figure, self.animate, init_func=self.init,
                                  frames=500, interval=30, blit=True)

    def init(self):
        self.line_x.set_data([], [])
        self.line_y.set_data([], [])
        self.line_z.set_data([], [])

        return self.line_x, self.line_y, self.line_z,\
            self.forcePlot.axes_x.xaxis, self.forcePlot.axes_y.xaxis, self.forcePlot.axes_z.xaxis

    def animate(self, i):
        self.line_x.set_data(self.t, self.x)
        self.line_y.set_data(self.t, self.y)
        self.line_z.set_data(self.t, self.z)

        return self.line_x, self.line_y, self.line_z,\
            self.forcePlot.axes_x.xaxis, self.forcePlot.axes_y.xaxis, self.forcePlot.axes_z.xaxis

    @QtCore.pyqtSlot(list, name='dataRead')
    def update_data(self, data):
        if data[0] > (self.intervalSize*self.currentFrame):
            self.currentFrame += 1
            self.move_to_interval(self.currentFrame)

        self.t.append(data[0])
        self.x.append(data[1])
        self.y.append(data[2])
        self.z.append(data[3])

    def move_to_interval(self, frame):
        self.currentFrame = frame

        # clear all arrays
        self.x = [0]
        self.y = [0]
        self.z = [0]
        self.t = [0]
        # set axis limits
        self.forcePlot.axes_x.set_xlim(self.intervalSize * (frame-1), self.intervalSize * frame)
        self.forcePlot.axes_y.set_xlim(self.intervalSize * (frame-1), self.intervalSize * frame)
        self.forcePlot.axes_z.set_xlim(self.intervalSize * (frame-1), self.intervalSize * frame)

    # MONKEY PATCH FUNCTION... to update ticks (uses figure.bbox rather than axes.bbox, may slow blitting significantly)
    def _blit_draw(self, artists, bg_cache):
        # Handles blitted drawing, which renders only the artists given instead
        # of the entire figure.
        updated_ax = []
        for a in artists:
            # If we haven't cached the background for this axes object, do
            # so now. This might not always be reliable, but it's an attempt
            # to automate the process.
            if a.axes not in bg_cache:
                # bg_cache[a.axes] = a.figure.canvas.copy_from_bbox(a.axes.bbox)
                # change here
                bg_cache[a.axes] = a.figure.canvas.copy_from_bbox(a.axes.figure.bbox)
            a.axes.draw_artist(a)
            updated_ax.append(a.axes)

        # After rendering all the needed artists, blit each axes individually.
        for ax in set(updated_ax):
            # and here
            # ax.figure.canvas.blit(ax.bbox)
            ax.figure.canvas.blit(ax.figure.bbox)


class HorizontalCanvas(FigureCanvas):

    def __init__(self, parent, title, ylabel, ylims):
        # Properties
        self.figure = Figure()

        FigureCanvas.__init__(self, self.figure)
        self.axes_l = self.figure.add_subplot(121)

        self.axes_l.set_xlabel('time(s)')
        self.axes_l.set_ylabel(ylabel)
        self.axes_l.set_xlim(0, 30)
        self.axes_l.set_ylim(ylims)
        self.axes_l.set_autoscale_on(False)

        self.axes_r = self.figure.add_subplot(122, sharey=self.axes_l)

        self.axes_r.set_xlabel('time(s)')
        self.axes_r.set_ylabel(ylabel)
        self.axes_r.set_xlim(0, 30)
        self.axes_r.set_ylim(ylims)
        self.axes_r.set_autoscale_on(False)
        matplotlib.artist.setp(self.axes_r.get_yticklabels(), visible=False)

        self.figure.suptitle(title)

        self.setParent(parent)

        # we define the widget as expandable
        FigureCanvas.setSizePolicy(self,
                                   QtWidgets.QSizePolicy.Expanding,
                                   QtWidgets.QSizePolicy.Expanding)
        # notify the system of updated policy
        FigureCanvas.updateGeometry(self)


class OrientationDisplayWidget(QtWidgets.QWidget):
    def __init__(self, parent):
        super(OrientationDisplayWidget, self).__init__(parent)

        #  data sources - initialised with len of 2
        self.L_x = [0.0, 0.0]
        self.L_y = [0.0, 0.0]
        self.L_z = [0.0, 0.0]
        self.R_x = [0.0, 0.0]
        self.R_y = [0.0, 0.0]
        self.R_z = [0.0, 0.0]
        self.L_t = [0.0, 0.0]
        self.R_t = [0.0, 0.0]
        self.currentFrame = 1
        self.intervalSize = 30  # in seconds
        self.ignored = 0

        self.label = QtWidgets.QLabel('', parent)
        self.plot = HorizontalCanvas(self, "Euler Rotation Left and Right", "Rotation (rad)", (-4, 4))

        self.anim = None

        self.Lline_x = Line2D([], [], color='blue')
        self.Lline_y = Line2D([], [], color='red')
        self.Lline_z = Line2D([], [], color='magenta')

        self.Rline_x = Line2D([], [], color='blue')
        self.Rline_y = Line2D([], [], color='red')
        self.Rline_z = Line2D([], [], color='magenta')

        self.plot.axes_l.add_line(self.Lline_x)
        self.plot.axes_l.add_line(self.Lline_y)
        self.plot.axes_l.add_line(self.Lline_z)

        self.plot.axes_r.add_line(self.Rline_x)
        self.plot.axes_r.add_line(self.Rline_y)
        self.plot.axes_r.add_line(self.Rline_z)

        self.plot.axes_l.xaxis.set_animated(True)
        self.plot.axes_r.xaxis.set_animated(True)

        self.layout = QtWidgets.QVBoxLayout()
        self.layout.addWidget(self.label)
        self.layout.addWidget(self.plot)
        self.plot.figure.canvas.draw()
        self.setLayout(self.layout)

    def start_anim(self):
        Animation._blit_draw = self._blit_draw
        self.anim = FuncAnimation(self.plot.figure, self.animate, init_func=self.init,
                                  frames=500, interval=30, blit=True)

    def init(self):
        self.Lline_x.set_data([], [])
        self.Lline_y.set_data([], [])
        self.Lline_z.set_data([], [])

        self.Rline_x.set_data([], [])
        self.Rline_y.set_data([], [])
        self.Rline_z.set_data([], [])

        return self.Lline_x, self.Lline_y, self.Lline_z,\
            self.Rline_x, self.Rline_y, self.Rline_z,\
            self.plot.axes_l.xaxis, self.plot.axes_r.xaxis

    def animate(self, i):

        self.Lline_x.set_data(self.L_t, self.L_x)
        self.Lline_y.set_data(self.L_t, self.L_y)
        self.Lline_z.set_data(self.L_t, self.L_z)

        self.Rline_x.set_data(self.R_t, self.R_x)
        self.Rline_y.set_data(self.R_t, self.R_y)
        self.Rline_z.set_data(self.R_t, self.R_z)

        return self.Lline_x, self.Lline_y, self.Lline_z,\
            self.Rline_x, self.Rline_y, self.Rline_z,\
            self.plot.axes_l.xaxis, self.plot.axes_r.xaxis

    # MONKEY PATCH FUNCTION... to update ticks (uses figure.bbox rather than axes.bbox, may slow blitting significantly)
    def _blit_draw(self, artists, bg_cache):
        # Handles blitted drawing, which renders only the artists given instead
        # of the entire figure.
        updated_ax = []
        for a in artists:
            # If we haven't cached the background for this axes object, do
            # so now. This might not always be reliable, but it's an attempt
            # to automate the process.
            if a.axes not in bg_cache:
                # bg_cache[a.axes] = a.figure.canvas.copy_from_bbox(a.axes.bbox)
                # change here
                bg_cache[a.axes] = a.figure.canvas.copy_from_bbox(a.axes.figure.bbox)
            a.axes.draw_artist(a)
            updated_ax.append(a.axes)

        # After rendering all the needed artists, blit each axes individually.
        for ax in set(updated_ax):
            # and here
            # ax.figure.canvas.blit(ax.bbox)
            ax.figure.canvas.blit(ax.figure.bbox)

    def move_to_interval(self, frame):
        self.currentFrame = frame
        self.ignored = 0

        # clear all arrays
        self.L_x = [0]
        self.L_y = [0]
        self.L_z = [0]
        self.L_t = [0]

        self.R_x = [0]
        self.R_y = [0]
        self.R_z = [0]
        self.R_t = [0]

        # set axis limits
        self.plot.axes_l.set_xlim(self.intervalSize * (frame-1), self.intervalSize * frame)
        self.plot.axes_r.set_xlim(self.intervalSize * (frame-1), self.intervalSize * frame)

    @QtCore.pyqtSlot(list, name='orientationRead')
    def update_data(self, data):

        if len(self.L_t) == 1 and self.ignored < 10:  # just moved interval ignore a few points(from last interval)
            self.ignored += 1
            return

        # data is time,RZ,RY,RX, side
        if data[0] > (self.intervalSize * self.currentFrame):
            self.currentFrame += 1
            self.move_to_interval(self.currentFrame)
        # w can never be 0
        if data[2] == 0.0:
            return

        sqx = data[3] * data[3]
        sqy = data[4] * data[4]
        sqz = data[5] * data[5]
        sqw = data[2] * data[2]

        Rx = atan2(2.0 * (data[3] * data[4] + data[5] * data[2]), (sqx - sqy - sqz + sqw))
        Ry = asin(-2.0 * (data[3] * data[5] - data[4] * data[2]) / (sqx + sqy + sqz + sqw))
        Rz = atan2(2.0 * (data[4] * data[5] + data[3] * data[2]), (-sqx - sqy + sqz + sqw))

        if data[1] == 0:
            self.L_t.append(data[0])
            self.L_x.append(Rx)
            self.L_y.append(Ry)
            self.L_z.append(Rz)
        else:
            self.R_t.append(data[0])
            self.R_x.append(Rx)
            self.R_y.append(Ry)
            self.R_z.append(Rz)


class ControlWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(ControlWindow, self).__init__()
        self.setGeometry(50, 50, 1800, 900)
        self.setWindowTitle("LapSimDevice")
        self.recording = False

        self.capture = Capture()

        # add menu actions
        # self.startForceAction = QtWidgets.QAction("&Toggle plotting", self)
        # self.startForceAction.setShortcut("Ctrl+S")
        # self.startForceAction.setStatusTip('Connect and start reading values from the 3D mouse')
        # self.startForceAction.triggered.connect(self.togglePlotting)

        # self.statusBar()
        # self.mainMenu = self.menuBar()
        # self.fileMenu = self.mainMenu.addMenu('&File')
        # self.fileMenu.addAction(self.startForceAction)

        # add layout with widgets to main window
        self.videoDisplayWidget = VideoDisplayWidget(self, self.capture.fps)
        self.forceDisplayWidget = ForceDisplayWidget(self)
        self.orientationDisplayWidget = OrientationDisplayWidget(self)

        self.h_widget = QtWidgets.QWidget(self)
        self.h_layout = QtWidgets.QHBoxLayout(self.h_widget)
        self.h_layout.addWidget(self.videoDisplayWidget)
        self.h_layout.addWidget(self.forceDisplayWidget)
        self.h_widget.setLayout(self.h_layout)

        self.v_widget = QtWidgets.QWidget(self)
        self.v_layout = QtWidgets.QVBoxLayout(self.v_widget)
        self.v_layout.addWidget(self.h_widget)
        self.v_layout.addWidget(self.orientationDisplayWidget)
        self.v_widget.setLayout(self.v_layout)

        self.setCentralWidget(self.v_widget)

        # resize window
        # TODO: resize to fit widgets dynamically ( their size is 30, 100 until drawn i think...

        # connect GUI elements with methods
        self.videoDisplayWidget.recordButton.clicked.connect(self.handle_recording)
        self.videoDisplayWidget.printButton.clicked.connect(self.print_values)

        # Start the capture
        if self.capture.cap.isOpened():
            self.capture.frameRead.connect(self.videoDisplayWidget.updateFrame)
            # needs to be a bit faster than the frame rate to ensure that each frame is saved
            self.captureThread = SensorThread(self.capture, 1000.0/40)
            self.captureThread.start()
            print('resolution: ', self.capture.cap.get(cv2.CAP_PROP_FRAME_HEIGHT),
                  self.capture.cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        else:
            self.capture = None
            self.videoDisplayWidget.videoLabel.setText('Camera not found!')

        # Start the force sensor on a seperate Thread
        self.spacemouse = Spacemouse()
        if self.spacemouse.dev is not None:
            self.spacemouseThread = SensorThread(self.spacemouse, 1000.0 / 100)
            self.spacemouseThread.start()
            self.spacemouse.dataRead.connect(self.forceDisplayWidget.update_data)
            self.forceDisplayWidget.start_anim()
        else:
            self.spacemouse = None
            self.forceDisplayWidget.label.setText('Spacemouse not found!')

        # Start the Arduino Thread
        self.arduino = Arduino()
        if self.arduino.ser is not None and self.arduino.ser.is_open:
            self.arduinoThread = SensorThread(self.arduino, 1000/150)
            self.arduinoThread.start()
            self.arduino.dataRead.connect(self.orientationDisplayWidget.update_data)
            self.orientationDisplayWidget.start_anim()
        else:
            self.arduino = None
            self.orientationDisplayWidget.label.setText('Arduino not found!')

    def handle_recording(self):
        # Start videoWriter
        if self.recording:  # SAVE
            self.recording = False
            if self.capture is not None:
                self.capture.save()
                print('saved Capture')
            if self.spacemouse is not None:
                self.spacemouse.save()
                print('saved Spacemouse')
            if self.arduino is not None:
                self.arduino.save()
                print('saved Arduino')
            self.videoDisplayWidget.recordButton.setText('Record')
            # print('saved')
        else:  # RECORD
            # TODO: add textbox to layout widget and use text to create folder for files each time record is clicked
            self.recording = True
            fold = self.videoDisplayWidget.textbox.text() + "\\"
            if not os.path.exists(fold):
                os.makedirs(fold)

            if self.capture is not None:
                self.capture.record(fold)
            else:
                print("VideoCapture didn't open successfully. Not Recording")

            # Start writing sensor values to csv
            if self.spacemouse is not None:
                self.forceDisplayWidget.move_to_interval(1)  # reset to first time interval i.e. 0-30s
                self.spacemouse.record(fold)
            if self.arduino is not None:
                ret = self.arduino.reset_orientation()
                if ret != 1:
                    print("Error starting orientation sensor! Orientation data will be inaccurate or missing!")
                self.orientationDisplayWidget.move_to_interval(1)
                self.arduino.record(fold)

            self.videoDisplayWidget.recordButton.setText('Save')

    def print_values(self):
        if self.arduino is not None:
            self.arduino.toggle_print()
            print("printing: " , self.arduino.printing)

    def stop_threads(self):
        self.stop_sensor(self.arduino)
        self.stop_sensor(self.capture)
        self.stop_sensor(self.spacemouse)
        print("stopped sensorThreads")

    @staticmethod
    def stop_sensor(sensor):
        if sensor is not None:
            sensor.stop()

    def closeEvent(self, event):
        print("closing...")
        self.stop_threads()
        event.accept()


if __name__ == '__main__':
    import sys
    # import cProfile
    # import pstats
    # from io import StringIO

    # pr = cProfile.Profile()
    # pr.enable()

    _excepthook = sys.excepthook


    def exception_hook(exctype, value, traceback):
        _excepthook(exctype, value, traceback)
        sys.exit(1)


    sys.excepthook = exception_hook

    app = QtWidgets.QApplication(sys.argv)
    window = ControlWindow()
    window.show()
    x = app.exec_()
    # pr.disable()
    # s = StringIO()
    # sortby = 'cumulative'
    # ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
    # ps.print_stats()
    # print(s.getvalue())

    sys.exit(x)
