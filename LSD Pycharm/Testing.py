from serial import *
import struct
from datetime import *


def getFileName(ext):
    seq = (str(datetime.now().strftime("%Y-%m-%d-%H%M%S")), ext)
    return ".".join(seq)


ser = Serial('COM5', 76800, timeout=1)
print(ser)
if not ser.is_open:
    ser.open()

f_d = open(getFileName('csv'), 'w')
f_d.write("t, x, y, squal\n")
f_d.flush()
now = datetime.now()


while True:
        try:
            byte = ser.read(1)
            if byte is b'd':  # check that it is the start of data
                t = ser.read(4)
                t, = struct.unpack('l', t)
                strng = str(t) + ","

                # read two data readings
                for i in range(1, 3):
                    # read X and Y shorts
                    for j in range(1, 3):
                        b = ser.read(2)
                        short, = struct.unpack('<h', b)
                        strng += str(short)
                        strng += ","
                    # read Squal unsigned char
                    s = ser.read(1)
                    squal, = struct.unpack('B', s)
                    strng += str(squal)
                    strng += ","

                # Read quaternion for left and right orientation sensors
                for bno in range(1, 3):
                    # read w, x, y, z of the quaternion
                    for i in range(1, 5):
                        b = ser.read(4)
                        flt, = struct.unpack('f', b)
                        strng += str(flt)
                        strng += ","

                print(strng)
                strng += "\n"
                f_d.write(strng)
                f_d.flush()
            elif byte is b'e':
                print('error on Arduino: %s' % "errcode")  #TODO: add error codes?
        except Exception as e:
            print(e)
