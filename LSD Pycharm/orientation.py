from serial import *
import struct
from datetime import *


ser = Serial('COM4', 38400, timeout=1)
print(ser)
if not ser.is_open:
    ser.open()
time = datetime.now()
interval = 8


def reset():
    written = ser.write(struct.pack('b', int(1)))
    if written > 1:
        print("Too many bytes written!")


while True:
    if (datetime.now()-time).total_seconds() > interval:
        time = datetime.now()
        reset()
        interval = 5
    else:
        byte = ser.read(1)
        if byte is b'L':
            strng = "L:"
            for i in range(1, 4):
                b = ser.read(4)
                flt, = struct.unpack('f', b)
                strng += str(flt)
                strng += ","
            print(strng)
        elif byte is b'R':
            strng = "R:"
            for i in range(1, 4):
                b = ser.read(4)
                flt, = struct.unpack('f', b)
                strng += str(flt)
                strng += ","
            print(strng)
        else:
            print(ser.readline())

