/*
 * This example bypasses the hardware motion interrupt pin
 * and polls the motion data registers at a fixed interval
 */

#include <SPI.h>
#include <avr/pgmspace.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
#include <EEPROM.h>

// Registers
#define Product_ID  0x00
#define Revision_ID 0x01
#define Motion  0x02
#define Delta_X_L 0x03
#define Delta_X_H 0x04
#define Delta_Y_L 0x05
#define Delta_Y_H 0x06
#define SQUAL 0x07
#define Raw_Data_Sum  0x08
#define Maximum_Raw_data  0x09
#define Minimum_Raw_data  0x0A
#define Shutter_Lower 0x0B
#define Shutter_Upper 0x0C
#define Control 0x0D
#define Config1 0x0F
#define Config2 0x10
#define Angle_Tune  0x11
#define Frame_Capture 0x12
#define SROM_Enable 0x13
#define Run_Downshift 0x14
#define Rest1_Rate_Lower  0x15
#define Rest1_Rate_Upper  0x16
#define Rest1_Downshift 0x17
#define Rest2_Rate_Lower  0x18
#define Rest2_Rate_Upper  0x19
#define Rest2_Downshift 0x1A
#define Rest3_Rate_Lower  0x1B
#define Rest3_Rate_Upper  0x1C
#define Observation 0x24
#define Data_Out_Lower  0x25
#define Data_Out_Upper  0x26
#define Raw_Data_Dump 0x29
#define SROM_ID 0x2A
#define Min_SQ_Run  0x2B
#define Raw_Data_Threshold  0x2C
#define Config5 0x2F
#define Power_Up_Reset  0x3A
#define Shutdown  0x3B
#define Inverse_Product_ID  0x3F
#define LiftCutoff_Tune3  0x41
#define Angle_Snap  0x42
#define LiftCutoff_Tune1  0x4A
#define Motion_Burst  0x50
#define LiftCutoff_Tune_Timeout 0x58
#define LiftCutoff_Tune_Min_Length  0x5A
#define SROM_Load_Burst 0x62
#define Lift_Config 0x63
#define Raw_Data_Burst  0x64
#define LiftCutoff_Tune2  0x65


// PMW3360 Properties
const int ssLeft = 9;
const int ssRight = 10;
int ss[2] = {ssLeft, ssRight};  // Slave Select for left and right
byte xydat[40]; // Mot, Xl, Xh, Yl, Yh, Squal

//Be sure to add the SROM file into this sketch via "Sketch->Add File"
extern const unsigned short firmware_length;
extern const unsigned char firmware_data[];


//BNO055 Properties
Adafruit_BNO055 bno[2] = { Adafruit_BNO055(0, 0x28), Adafruit_BNO055(1, 0x29) };
unsigned int resetByte = 0;
// set in relative orientation mode IMUPLUS
Adafruit_BNO055::adafruit_bno055_opmode_t mode = Adafruit_BNO055::OPERATION_MODE_IMUPLUS;
imu::Quaternion quat[2] = {};

// Timer/Shared Properties
unsigned long startTime;
unsigned long readTime;
bool takeReading = false;
unsigned char readNo = 0; //read number 0 indexed
bool sendSerial = false;

// Error Handling
const char error = 'e';
const unsigned char error_starting = 1;

//https://stackoverflow.com/questions/24420246/c-function-to-convert-float-to-byte-array
union{
  float f;
  unsigned char bytes[4]; 
} floatToBytes;

union{
  unsigned long l;
  unsigned char bytes[4]; 
} longToBytes;


void setup() {
  Serial.begin(250000);

  // start the PMW3360 sensors
  for(int i=0; i<2; i++){
    pinMode (ss[i], OUTPUT);
  }
  
  
  SPI.begin();
  SPI.beginTransaction(SPISettings(16000000, MSBFIRST, SPI_MODE3));
  performStartup();  

  //Initialise the BNO055 sensors 
    if (!bno[0].begin(mode) || !bno[1].begin(mode))
    {
        // There was a problem detecting the BNO055 ... check your connections
        Serial.write(error);
        Serial.write(error_starting);
        while (1);
    }

  // Start the timer1 Interupt at 200Hz 
  start_timer();

  // write any value to motion_burst register to activate
  for(int i=0; i<2; i++){
    adns_write_reg(0x50, 0x21, ss[i]);
  }
  readTime = startTime = millis();
}

void loop() {  

  //  Reset BNO055 if signal recieved
  if(Serial.available() > 0){
    resetByte = Serial.read();
    if(resetByte == 1){
      startTime = millis();
      if (!bno[0].begin(mode) || !bno[1].begin(mode)) {
        // signal to program that there was an error starting one of the sensors!
        Serial.write(error);
        Serial.write(error_starting);
        while (1);
      }
      resetByte = 0;
    }
  }
  
  // going at speed of timer1
  if (takeReading){
    
      // read both PMW3360s
      ReadMotionBurst();
      // read BNO055 (Left then Right)
      readOrientation();
      
      readNo++;
      takeReading = false;
  }
  
  // going at quarter speed of timer1
  if(sendSerial){
    readTime = millis() - startTime;
    // Send Data Packet 
    Serial.print('d'); // signal start of data
    writeLong(readTime); // write time stamp
    Serial.write(xydat, 40); // delX, delY, squal for both sides and all 4 readings from PMW3360s
    for (int i=0; i<2; i++){  // write quaternions for both sides from BNO055 
      writeFloat(quat[i].w());
      writeFloat(quat[i].x());
      writeFloat(quat[i].y());
      writeFloat(quat[i].z());
    }
    Serial.flush();
    
    sendSerial = false;
    readNo = 0;
  }
}

void readOrientation(){
  if(readNo == 0){ 
    quat[0] = bno[0].getQuat();
  } else if(readNo % 2 == 0) { // if even has to be two (readNo 0-3)
    quat[1] = bno[1].getQuat();
  }
    
    // Display the floating point data 
    //   v[0] is applied 1st about z (ie, roll)
    //   v[1] is applied 2nd about y (ie, pitch)
    //   v[2] is applied 3rd about x (ie, yaw)
}

void start_timer(){
  cli(); 
  //set timer1 interrupt at 200Hz
  TCCR1A = 0; // set entire TCCR1A register to 0
  TCCR1B = 0; // same for TCCR1B
  TCNT1  = 0; //initialize counter value to 0
  // set compare match register for 200hz increments
  OCR1A = 9999; // = (16*10^6) / (200*8) - 1 (must be <65536) 9999 = 200Hz
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS11 for 8 prescaler
  TCCR1B |= (1 << CS11);  
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);
  sei();
}

void adns_com_begin(int lss){
  digitalWrite(lss, LOW);
}

void adns_com_end(int lss){
  digitalWrite(lss, HIGH);
}

byte adns_read_reg(byte reg_addr, int lss){
  adns_com_begin(lss);
  
  // send adress of the register, with MSBit = 0 to indicate it's a read
  SPI.transfer(reg_addr & 0x7f );
  delayMicroseconds(100); // tSRAD
  // read data
  byte data = SPI.transfer(0);
  
  delayMicroseconds(1); // tSCLK-NCS for read operation is 120ns
  adns_com_end(lss);
  delayMicroseconds(19); //  tSRW/tSRR (=20us) minus tSCLK-NCS

  return data;
}

void adns_write_reg(byte reg_addr, byte data, int lss){
  adns_com_begin(lss);
  
  //send adress of the register, with MSBit = 1 to indicate it's a write
  SPI.transfer(reg_addr | 0x80 );
  //sent data
  SPI.transfer(data);
  
  delayMicroseconds(20); // tSCLK-NCS for write operation
  adns_com_end(lss);
  delayMicroseconds(100); // tSWW/tSWR (=120us) minus tSCLK-NCS. Could be shortened, but is looks like a safe lower bound 
}

void adns_upload_firmware(int lss){
  // send the firmware to the chip, cf p.18 of the datasheet
  
  // write 0x1d in SROM_enable reg for initializing
  adns_write_reg(SROM_Enable, 0x1d, lss); 
  
  // wait for more than one frame period
  delay(10); // assume that the frame rate is as low as 100fps... even if it should never be that low
  
  // write 0x18 to SROM_enable to start SROM download
  adns_write_reg(SROM_Enable, 0x18, lss); 
  
  // write the SROM file (=firmware data) 
  adns_com_begin(lss);
  SPI.transfer(SROM_Load_Burst | 0x80); // write burst destination adress
  delayMicroseconds(15);
  
  // send all bytes of the firmware
  unsigned char c;
  for(int i = 0; i < firmware_length; i++){ 
    c = (unsigned char)pgm_read_byte(firmware_data + i);
    SPI.transfer(c);
    delayMicroseconds(15);
  }

  adns_com_end(lss);
  
  }

void performStartup(void){
    // ensure that the serial port is reset
    for(int i=0; i<2; i++){
      
      adns_com_end(ss[i]); 
      adns_com_begin(ss[i]);
      adns_com_end(ss[i]); 
      
      // force reset
      adns_write_reg(Power_Up_Reset, 0x5a, ss[i]); 
  
      delay(50); // wait for it to reboot
      // read registers 0x02 to 0x06 (and discard the data)
      adns_read_reg(Motion, ss[i]);
      adns_read_reg(Delta_X_L, ss[i]);
      adns_read_reg(Delta_X_H, ss[i]);
      adns_read_reg(Delta_Y_L, ss[i]);
      adns_read_reg(Delta_Y_H, ss[i]);
  
      // upload the firmware
      adns_upload_firmware(ss[i]);
  
      //Read the SROM_ID register to verify the ID before any other register reads or writes.
      adns_read_reg(SROM_ID, ss[i]);
  
      // Write 0 to Rest_En bit and Rpt_Mod of Config2 register to disable Rest mode and make resolution apply to x and y.
      adns_write_reg(Config2, 0x00, ss[i]);

      // Write resolution register to override default
      adns_write_reg(Config1, 0x77, ss[i]);  // max is 0x77 == 12000 CPI
      delay(10);
    
    }
  }

void ReadMotionBurst(void){

//  read first 7 bytes
//    Motion
//    Observation
//    Delta_X_L
//    Delta_X_H
//    Delta_Y_L
//    Delta_Y_H
//    SQUAL

  for(int i=0; i<2; i++){
    adns_com_begin(ss[i]);
    unsigned char index = 5*(i+(2*readNo)); // 0, 5, 10, 15 etc. 
    //send adress of the motion_burst register
    SPI.transfer(0x50);

    delayMicroseconds(35);  // tSRAD_MOTBR

    SPI.transfer(0);  // discard MOT
    SPI.transfer(0); // discard observation (used for recovery)
    xydat[index] = SPI.transfer(0); // Xl
    xydat[index +1] = SPI.transfer(0);  //Xh
    xydat[index +2] = SPI.transfer(0); // Yl
    xydat[index +3] = SPI.transfer(0); //Yh
    xydat[index +4] = SPI.transfer(0); //Squal

    adns_com_end(ss[i]);
    delayMicroseconds(1);
  }
  
//
//    for(int i=0; i<2; i++){
//      adns_com_begin(ss[i]);
//
//      //send adress of the motion_burst register
//      SPI.transfer(0x50);
//
//      delayMicroseconds(35);  // tSRAD_MOTBR
//  
//      SPI.transfer(0);  // discard MOT
//      SPI.transfer(0); // discard observation (used for recovery)
//      xydat[5*(i+2)] = SPI.transfer(0); // Xl
//      xydat[5*(i+2) +1] = SPI.transfer(0);  //Xh
//      xydat[5*(i+2) +2] = SPI.transfer(0); // Yl
//      xydat[5*(i+2) +3] = SPI.transfer(0); //Yh
//      xydat[5*(i+2) +4] = SPI.transfer(0); //Squal
//
//      adns_com_end(ss[i]);
//      delayMicroseconds(1);
//    }
  
}

ISR(TIMER1_COMPA_vect){ //timer1 interrupt 200Hz  
  if(readNo == 3){
    sendSerial = true;
  }
  takeReading = true;
}

void writeLong(long l) {  
    longToBytes.l = l;
    Serial.write(longToBytes.bytes, 4);
    return;
  }

void writeFloat(float f) {  
  floatToBytes.f = f;
  Serial.write(floatToBytes.bytes, 4);
  Serial.flush();
  return;
}

