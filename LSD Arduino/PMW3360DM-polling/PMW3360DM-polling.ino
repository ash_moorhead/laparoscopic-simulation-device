/*
 * This example bypasses the hardware motion interrupt pin
 * and polls the motion data registers at a fixed interval
 */

#include <SPI.h>
#include <avr/pgmspace.h>

// Registers
#define Product_ID  0x00
#define Revision_ID 0x01
#define Motion  0x02
#define Delta_X_L 0x03
#define Delta_X_H 0x04
#define Delta_Y_L 0x05
#define Delta_Y_H 0x06
#define SQUAL 0x07
#define Raw_Data_Sum  0x08
#define Maximum_Raw_data  0x09
#define Minimum_Raw_data  0x0A
#define Shutter_Lower 0x0B
#define Shutter_Upper 0x0C
#define Control 0x0D
#define Config1 0x0F
#define Config2 0x10
#define Angle_Tune  0x11
#define Frame_Capture 0x12
#define SROM_Enable 0x13
#define Run_Downshift 0x14
#define Rest1_Rate_Lower  0x15
#define Rest1_Rate_Upper  0x16
#define Rest1_Downshift 0x17
#define Rest2_Rate_Lower  0x18
#define Rest2_Rate_Upper  0x19
#define Rest2_Downshift 0x1A
#define Rest3_Rate_Lower  0x1B
#define Rest3_Rate_Upper  0x1C
#define Observation 0x24
#define Data_Out_Lower  0x25
#define Data_Out_Upper  0x26
#define Raw_Data_Dump 0x29
#define SROM_ID 0x2A
#define Min_SQ_Run  0x2B
#define Raw_Data_Threshold  0x2C
#define Config5 0x2F
#define Power_Up_Reset  0x3A
#define Shutdown  0x3B
#define Inverse_Product_ID  0x3F
#define LiftCutoff_Tune3  0x41
#define Angle_Snap  0x42
#define LiftCutoff_Tune1  0x4A
#define Motion_Burst  0x50
#define LiftCutoff_Tune_Timeout 0x58
#define LiftCutoff_Tune_Min_Length  0x5A
#define SROM_Load_Burst 0x62
#define Lift_Config 0x63
#define Raw_Data_Burst  0x64
#define LiftCutoff_Tune2  0x65

#define SAMPLERATE_DELAY_MS (2)

//Set this to what pin your "INT0" hardware interrupt feature is on
#define Motion_Interrupt_Pin 9

const int ncs = 10;  //This is the SPI "slave select" pin that the sensor is hooked up to

// Mot, Xl, Xh, Yl, Yh, Squal
byte xydat[10];
unsigned long startTime;
bool takeReading = false;
bool firstRead = true;
bool sendSerial = false;

union{
  unsigned long l;
  unsigned char bytes[4]; 
} longToBytes;

//Be sure to add the SROM file into this sketch via "Sketch->Add File"
extern const unsigned short firmware_length;
extern const unsigned char firmware_data[];

void setup() {
  Serial.begin(38400);
  
  pinMode (ncs, OUTPUT);
  
  SPI.begin();
//  SPI.setDataMode(SPI_MODE3);
//  SPI.setBitOrder(MSBFIRST);
  SPI.beginTransaction(SPISettings(16000000, MSBFIRST, SPI_MODE3));
  
  performStartup();  

  cli();
  
  //set timer1 interrupt at 100Hz
  TCCR1A = 0;// set entire TCCR1A register to 0
  TCCR1B = 0;// same for TCCR1B
  TCNT1  = 0;//initialize counter value to 0
  // set compare match register for 200hz increments
  OCR1A = 9999;// = (16*10^6) / (200*8) - 1 (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS11 for 8 prescaler
  TCCR1B |= (1 << CS11);  
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);

  sei();

  // write any value to motion_burst register to begin
  adns_write_reg(0x50, 0x21);
  startTime = millis();
}

void adns_com_begin(){
  digitalWrite(ncs, LOW);
}

void adns_com_end(){
  digitalWrite(ncs, HIGH);
}

byte adns_read_reg(byte reg_addr){
  adns_com_begin();
  
  // send adress of the register, with MSBit = 0 to indicate it's a read
  SPI.transfer(reg_addr & 0x7f );
  delayMicroseconds(100); // tSRAD
  // read data
  byte data = SPI.transfer(0);
  
  delayMicroseconds(1); // tSCLK-NCS for read operation is 120ns
  adns_com_end();
  delayMicroseconds(19); //  tSRW/tSRR (=20us) minus tSCLK-NCS

  return data;
}

void adns_write_reg(byte reg_addr, byte data){
  adns_com_begin();
  
  //send adress of the register, with MSBit = 1 to indicate it's a write
  SPI.transfer(reg_addr | 0x80 );
  //sent data
  SPI.transfer(data);
  
  delayMicroseconds(20); // tSCLK-NCS for write operation
  adns_com_end();
  delayMicroseconds(100); // tSWW/tSWR (=120us) minus tSCLK-NCS. Could be shortened, but is looks like a safe lower bound 
}

void adns_upload_firmware(){
  // send the firmware to the chip, cf p.18 of the datasheet
  
  // write 0x1d in SROM_enable reg for initializing
  adns_write_reg(SROM_Enable, 0x1d); 
  
  // wait for more than one frame period
  delay(10); // assume that the frame rate is as low as 100fps... even if it should never be that low
  
  // write 0x18 to SROM_enable to start SROM download
  adns_write_reg(SROM_Enable, 0x18); 
  
  // write the SROM file (=firmware data) 
  adns_com_begin();
  SPI.transfer(SROM_Load_Burst | 0x80); // write burst destination adress
  delayMicroseconds(15);
  
  // send all bytes of the firmware
  unsigned char c;
  for(int i = 0; i < firmware_length; i++){ 
    c = (unsigned char)pgm_read_byte(firmware_data + i);
    SPI.transfer(c);
    delayMicroseconds(15);
  }

  adns_com_end();
  
  }

void performStartup(void){
  adns_com_end(); // ensure that the serial port is reset
  adns_com_begin(); // ensure that the serial port is reset
  adns_com_end(); // ensure that the serial port is reset
  adns_write_reg(Power_Up_Reset, 0x5a); // force reset
  delay(50); // wait for it to reboot
  // read registers 0x02 to 0x06 (and discard the data)
  adns_read_reg(Motion);
  adns_read_reg(Delta_X_L);
  adns_read_reg(Delta_X_H);
  adns_read_reg(Delta_Y_L);
  adns_read_reg(Delta_Y_H);
  // upload the firmware
  adns_upload_firmware();

  //Read the SROM_ID register to verify the ID before any other register reads or writes.
  adns_read_reg(SROM_ID);

  // Write 0 to Rest_En bit and Rpt_Mod of Config2 register to disable Rest mode and make resolution apply to x and y.
  adns_write_reg(Config2, 0x00);

  // Write resolution register to override default
  adns_write_reg(Config1, 0x77);  // max is 0x77 == 12000 CPI
  delay(10);
  }

void ReadMotion(void){

//    if mot bit is set data ready to read
      xydat[0] = adns_read_reg(Delta_X_L);
      xydat[1] = adns_read_reg(Delta_X_H);
      xydat[2] = adns_read_reg(Delta_Y_L);
      xydat[3] = adns_read_reg(Delta_Y_H);
 
  }

void ReadMotionBurst(void){

  adns_com_begin();

  //send adress of the motion_burst register
  SPI.transfer(0x50);

  delayMicroseconds(35);  // tSRAD_MOTBR

//  read first 7 bytes
//    Motion
//    Observation
//    Delta_X_L
//    Delta_X_H
//    Delta_Y_L
//    Delta_Y_H
//    SQUAL

  if (firstRead){
    SPI.transfer(0);  // discard MOT
    SPI.transfer(0); // discard observation (used for recovery)
    xydat[0] = SPI.transfer(0); // Xl
    xydat[1] = SPI.transfer(0);  //Xh
    xydat[2] = SPI.transfer(0); // Yl
    xydat[3] = SPI.transfer(0); //Yh
    xydat[4] = SPI.transfer(0); //Squal
    firstRead = false;
  } else {
    SPI.transfer(0);  // discard MOT
    SPI.transfer(0); // discard observation (used for recovery)
    xydat[5] = SPI.transfer(0); // Xl
    xydat[6] = SPI.transfer(0);  //Xh
    xydat[7] = SPI.transfer(0); // Yl
    xydat[8] = SPI.transfer(0); //Yh
    xydat[9] = SPI.transfer(0); //Squal
  }
  
  adns_com_end();
  delayMicroseconds(1);
}

int convTwosComp(int b){
  //Convert from 2's complement
  if(b & 0x80){
    b = -1 * ((b ^ 0xff) + 1);
    }
  return b;
  }  

ISR(TIMER1_COMPA_vect){ //timer1 interrupt 200Hz
  if (!firstRead){
    sendSerial = true; 
  }
  
  takeReading = true;
}

void loop() {  
  
  if (takeReading){
      ReadMotionBurst();
      takeReading = false;
  }

  if(sendSerial){

    Serial.print('L');
    writeLong(millis() - startTime); // TODO
    Serial.write(xydat, 10);
    Serial.flush();
   
    sendSerial = false;
    firstRead = true;
  }
}

  void writeLong(long l) {  
    longToBytes.l = l;
    Serial.write(longToBytes.bytes, 4);
    return;
  }

