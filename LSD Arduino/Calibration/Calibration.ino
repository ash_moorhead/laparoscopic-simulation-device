#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
#include <EEPROM.h>

#define BNO055_SAMPLERATE_DELAY_MS (20) 
  
Adafruit_BNO055 bno[2] = { Adafruit_BNO055(0, 0x28), Adafruit_BNO055(1, 0x29) };
unsigned int resetByte = 0;
const unsigned char error = 'e';
Adafruit_BNO055::adafruit_bno055_opmode_t mode = Adafruit_BNO055::OPERATION_MODE_IMUPLUS;
char side = "A";

//https://stackoverflow.com/questions/24420246/c-function-to-convert-float-to-byte-array
union{
  float f;
  unsigned char bytes[4]; 
} floatToBytes;

void setup(void) 
{
    Serial.begin(38400);
    delay(1000);

    /* Initialise the sensor */
    if (!bno[0].begin(mode) || !bno[1].begin(mode))
    {
        /* There was a problem detecting the BNO055 ... check your connections */
        Serial.print("Ooops, no BNO055's detected ... Check your wiring or I2C ADDR!");
        Serial.write(error);
        while (1);
    }


// if using NDOF or NDOF_FMC_OFF
   performCalibration(bno, sizeof(bno));
  
}

void loop(void) 
{
  //  Reset sensor if signal recieved
  if(Serial.available() > 0){
    resetByte = Serial.read();
    if(resetByte == 1){
      if (!bno[0].begin(mode) || !bno[1].begin(mode)) {
        // signal to program that there was an error starting one of the sensors!
        Serial.write(error);
        while (1);
      }
      resetByte = 0;
    }
  }
  
  for(int i=0; i < (sizeof(bno) / sizeof(Adafruit_BNO055)); i++)
  {
    imu::Quaternion quat = bno[i].getQuat();
    imu::Vector<3> euler = quat.toEuler();
    /* Display the floating point data */
    //   v[0] is applied 1st about z (ie, roll)
    //   v[1] is applied 2nd about y (ie, pitch)
    //   v[2] is applied 3rd about x (ie, yaw)
    side = (i==1) ? 'L' : 'R';  
    Serial.print(side);
    writeFloat(euler.x());
    writeFloat(euler.y());
    writeFloat(euler.z());
  }

  delay(BNO055_SAMPLERATE_DELAY_MS);
}

void writeFloat(float f) {  
  floatToBytes.f = f;
  Serial.write(floatToBytes.bytes, 4);
  Serial.flush();
  return;
}

void performCalibration(Adafruit_BNO055 bno[], int arrayLen)
{
    int eeAddress = 0;
    long bnoID[2];
    bool foundCalib[2] = {false, false};
    adafruit_bno055_offsets_t calibrationData[2];
    sensor_t sensor[2];
    sensors_event_t event[2];

    for(int i=0; i < (arrayLen / sizeof(Adafruit_BNO055)); i++)
    { 
        Serial.print("Calibrating sensor: ");
        Serial.println(i);
        
        EEPROM.get(eeAddress, bnoID[i]);
  
        /*
        *  Look for the sensor's unique ID at the beginning oF EEPROM.
        *  This isn't foolproof, but it's better than nothing.
        */
        bno[i].getSensor(&sensor[i]);
        
        if (bnoID[i] == sensor[i].sensor_id)
        {
            //  Found Calibration for this sensor in EEPROM
            eeAddress += sizeof(long);
            EEPROM.get(eeAddress, calibrationData[i]);
  
            //   Restoring Calibration data to the BNO055
            bno[i].setSensorOffsets(calibrationData[i]);
  
            foundCalib[i] = true;
        }
  
        delay(200);

       //Crystal must be configured AFTER loading calibration data into BNO055.
        bno[i].setExtCrystalUse(true);
    
        bno[i].getEvent(&event[i]);
        if (foundCalib[i]){
            Serial.println("Move sensor slightly to calibrate magnetometers");
            while (!bno[i].isFullyCalibrated())
            {
                bno[i].getEvent(&event[i]);
                displayCalStatus(bno[i], i);
                delay(BNO055_SAMPLERATE_DELAY_MS);
            }
        }
        else
        {
            Serial.print("Please Calibrate Sensor: ");
            Serial.println(i);
            while (!bno[i].isFullyCalibrated())
            {
                bno[i].getEvent(&event[i]);

                /* Optional: Display calibration status */
                displayCalStatus(bno[i], i);

                /* Wait the specified delay before requesting new data */
                delay(BNO055_SAMPLERATE_DELAY_MS);
            }
        }
        Serial.print("device ");
        Serial.print(i);
        Serial.println(" is fully calibrated!");

        adafruit_bno055_offsets_t newCalib;
        bno[i].getSensorOffsets(newCalib);

        //  Storing calibration data to EEPROM
        if(i == 0){
            eeAddress = 0;
            
            bno[i].getSensor(&sensor[i]);
            bnoID[i] = sensor[i].sensor_id;
            EEPROM.put(eeAddress, bnoID[i]);

            eeAddress += sizeof(long);
            EEPROM.put(eeAddress, newCalib);

            eeAddress += sizeof(newCalib);
        } else {
            eeAddress = sizeof(long) + sizeof(newCalib);
            
            bno[i].getSensor(&sensor[i]);
            bnoID[i] = sensor[i].sensor_id;
            EEPROM.put(eeAddress, bnoID[i]);

            eeAddress += sizeof(long);
            EEPROM.put(eeAddress, newCalib);    
        }

        Serial.println("Data stored to EEPROM.");
    
        delay(500);
    }

  Serial.println("Calibration complete");
}

void displayCalStatus(Adafruit_BNO055 bno, int id)
{
  uint8_t sys, gyro, accel, mag;
  sys = gyro = accel = mag = 0;
  
  bno.getCalibration(&sys, &gyro, &accel, &mag);

  Serial.print(id);
  Serial.print("\t");
  if (!system)
  {
    Serial.print("! ");
  }
  /* Display the individual values */
  Serial.print("Sys:");
  Serial.print(sys, DEC);
  Serial.print(" G:");
  Serial.print(gyro, DEC);
  Serial.print(" A:");
  Serial.print(accel, DEC);
  Serial.print(" M:");
  Serial.println(mag, DEC);

}

void displaySensorDetails(Adafruit_BNO055 bno)
{
    sensor_t sensor;
    bno.getSensor(&sensor);
    Serial.println("------------------------------------");
    Serial.print("Sensor:       "); Serial.println(sensor.name);
    Serial.print("Driver Ver:   "); Serial.println(sensor.version);
    Serial.print("Unique ID:    "); Serial.println(sensor.sensor_id);
    Serial.print("Max Value:    "); Serial.print(sensor.max_value); Serial.println(" xxx");
    Serial.print("Min Value:    "); Serial.print(sensor.min_value); Serial.println(" xxx");
    Serial.print("Resolution:   "); Serial.print(sensor.resolution); Serial.println(" xxx");
    Serial.println("------------------------------------");
    Serial.println("");
    delay(500);
}

void displaySensorStatus(Adafruit_BNO055 bno)
{
    /* Get the system status values (mostly for debugging purposes) */
    uint8_t system_status, self_test_results, system_error;
    system_status = self_test_results = system_error = 0;
    bno.getSystemStatus(&system_status, &self_test_results, &system_error);

    /* Display the results in the Serial Monitor */
    Serial.println("");
    Serial.print("System Status: 0x");
    Serial.println(system_status, HEX);
    Serial.print("Self Test:     0x");
    Serial.println(self_test_results, HEX);
    Serial.print("System Error:  0x");
    Serial.println(system_error, HEX);
    Serial.println("");
    delay(500);
}

void displaySensorOffsets(const adafruit_bno055_offsets_t &calibData)
{
    Serial.print("Accelerometer: ");
    Serial.print(calibData.accel_offset_x); Serial.print(" ");
    Serial.print(calibData.accel_offset_y); Serial.print(" ");
    Serial.print(calibData.accel_offset_z); Serial.print(" ");

    Serial.print("\nGyro: ");
    Serial.print(calibData.gyro_offset_x); Serial.print(" ");
    Serial.print(calibData.gyro_offset_y); Serial.print(" ");
    Serial.print(calibData.gyro_offset_z); Serial.print(" ");

    Serial.print("\nMag: ");
    Serial.print(calibData.mag_offset_x); Serial.print(" ");
    Serial.print(calibData.mag_offset_y); Serial.print(" ");
    Serial.print(calibData.mag_offset_z); Serial.print(" ");

    Serial.print("\nAccel Radius: ");
    Serial.print(calibData.accel_radius);

    Serial.print("\nMag Radius: ");
    Serial.print(calibData.mag_radius);
}
